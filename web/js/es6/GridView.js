/**
 * Created by francis on 17-6-6.
 */
class GridView extends AbstractView{
    constructor(pagination, options){
        super(pagination, options);
        this.name = 'grid';
    }
    
    _models2HtmlStr(models){
        var self = this;
        var $container = $('<div>').addClass('ui four stackable cards');
        var htmlStr = '';
        for(var model of models){
            var itemHtmlStr = `<div class="ui card">
          <div class="ui slide masked reveal image">
            <img src="${model.img}">
          </div>
          <div class="content">
            <a class="header">${model.name}</a>
            <div class="meta">
              <span class="date">${model.price}</span>
            </div>
          </div>
        </div><!--end item-->`;
            htmlStr = htmlStr + itemHtmlStr;
        }
        $container.append(htmlStr);
        return $container;
    }
}