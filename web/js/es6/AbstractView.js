/**
 * Created by francis on 17-6-4.
 */
class AbstractView{
    constructor(pagination, options){
        this.settings = $.extend({listContainer:'#itemlist',pagerCountainer:'.pager',
            totalCount:'.total .count'}, options||{});
        this.pagination = pagination;
        this.name = 'abstract';
    }
    static get noSearchData(){
        return `<li class="no-data">当前查询条件下没有信息，去试试其他查询条件吧！</li>`;
    }

    displayTotalCount(){
        console.log('this.pagination.totalCount:',this.pagination.totalCount);
        $(this.settings.totalCount).text(this.pagination.totalCount);
    }

    displayPager(){
        var $pages = this._pageButtons();
        $(this.settings.pagerCountainer).empty().append($pages);
    }

    _pageButtons(){
        var $container = $('<div>').addClass('ui pagination menu');
        var pagerange = this.pagination.pageRange;
        for(var i = pagerange[0]; i <= pagerange[1]; i++){
            var $btn = $('<a>').addClass('item').text(i+1);
            if(i == this.pagination.page){
                $btn.addClass('active');
            }
            $container.append($btn);
        }
        return $container;
    }

    replaceProducts(items){
        var self = this;
        $(self.settings.listContainer).empty().removeAttr('style');
        var htmlString = this._models2HtmlStr(items);
        if(htmlString)
            $(self.settings.listContainer).html(htmlString);
        else{
            $(self.settings.listContainer).html(this.constructor.noSearchData);
        }
        window.scrollTo(0,0);
    }

    appendProducts(items){
        var self = this;
        var htmlString = this._models2HtmlStr(items);
        if(htmlString)
            $(self.settings.listContainer).append(htmlString);
    }

    display(items, append=true){
        this.displayTotalCount();
        if(append){
            this.appendProducts(items);
        }else{
            this.replaceProducts(items);
        }
        this.displayPager();
    }

    _models2HtmlStr(models){

    }
}