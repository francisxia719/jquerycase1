/**
 * Created by francis on 17-6-5.
 */
class Model{
    constructor(pagination){
        var self = this;
        self.data_file = 'data.json';
        self.pagination = pagination;
        self.data = $.getJSON(self.data_file);
        self.cache = {};
        self.cache.last = null; //last found items, pagination pageSized items.
        self.cache.all = null;
        self.cache.food = null;
        self.cache.movie = null;
    }

    _paginationCut(data, condition){
        var self = this;
        self.pagination.totalCount = data.length;
        self.pagination.page = condition.page;
        console.log('self.pagination.page:', self.pagination.page);
        console.log('self.pagination.offset:', self.pagination.offset);
        var pagination = self.pagination;
        console.log('pagination.offset:', pagination.offset);
        data = data.slice(pagination.offset, pagination.offset+pagination.limit);
        self.cache.last = data;
        return data;
    }

    find(condition){
        var self = this;
        var defer = $.Deferred();
        if(condition.type){
            this.findType(condition.type).then(function(data){
                data = self._paginationCut(data, condition);
                defer.resolve(data);
            });
        }else{
            this.findAll().then(function(data){
                data = self._paginationCut(data, condition);
                defer.resolve(data);
            });
        }
        return defer;
    }

    findAll(){
        var self = this;
        var defer = $.Deferred();
        if(self.cache.all){
            self.pagination.totalCount = self.cache.all.length;
            defer.resolve(self.cache.all);
        }else{
            self.data.done(function(data){
                var items = data.items;
                self.cache.all = items;
                self.pagination.totalCount = self.cache.all.length;
                defer.resolve(self.cache.all);
            });
        }
        return defer;
    }

    findType(type){
        var self = this;
        var defer = $.Deferred();
        if(self.cache[type]){
            self.pagination.totalCount = self.cache[type].length;
            defer.resolve(self.cache[type]);
        }else{
            self.data.done(function(data){
                var items = data.items.filter(function(elem, index, self) {
                    return elem.type == type;
                });
                self.cache[type] = items;
                self.pagination.totalCount = items.length;
                defer.resolve(self.cache[type]);
            });
        }
        return defer;
    }
}