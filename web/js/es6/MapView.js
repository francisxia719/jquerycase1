/**
 * Created by francis on 17-6-6.
 */
class MapView{
    constructor(pagination, options){
        this.settings = $.extend({container:'itemlist',containerHeight:600,pagerCountainer:'.pager',
            totalCount:'.total .count'}, options||{});
        this.pagination = pagination;
        this.name = 'map';
    }

    init(){
        $(this.settings.pagerCountainer).empty();
        $('#'+this.settings.container).height(this.settings.containerHeight);
        this.map = new BMap.Map(this.settings.container);
        this.map.enableScrollWheelZoom(true);
        this.centerPoint = new BMap.Point(116.404, 39.915);
        this.map.centerAndZoom(this.centerPoint, 15);
    }

    displayTotalCount(){
        console.log('this.pagination.totalCount:',this.pagination.totalCount);
        $(this.settings.totalCount).text(this.pagination.totalCount);
    }

    display(items){
        this.displayTotalCount();
        this.addMarks(items);
    }

    addMarks(items){
        var self = this;
        self.map.clearOverlays();
        for(var item of items){
            var point = new BMap.Point(item.coord.x, item.coord.y);
            var html = {};
            html.food = `<div class="mapmarker ${item.type}"><i class="food icon"></i>${item.name} ${item.price}</div>`;
            html.movie = `<div class="mapmarker ${item.type}"><i class="record icon"></i>${item.name} ${item.price}</div>`;
            var marker = new BMapLib.RichMarker(html[item.type], point);
            self.map.addOverlay(marker);
        }

    }
/*    _addFoodMarks(items){

    }
    _addMovieMarks(items){

    }*/
}