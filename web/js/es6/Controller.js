/**
 * Created by francis on 17-6-6.
 */
class Controller{
    constructor(model, view, type){
        this._view = view;
        this.model = model;
        this._type = type;
    }

    /**
     * render index page
     * @param integer page current page number
     * @param string type
     */
    index(page){
        var self = this;
        if(self._view.name == 'map'){
            if(this._type){
                self.model.findType(this._type).then(function(data){
                    self._view.display(data, false);
                });
            }else{
                self.model.findAll().then(function(data){
                    self._view.display(data, false);
                });
            }
        }else{
            self.model.find({page:page,type:self._type}).then(function(data){
                self._view.display(data, false);
            });
        }
    }

    set type(val){
        this._type = val;
    }

    set view(view){
        var self = this;
        this._view = view;
        if(self._view.name == 'map'){
            self._view.init();
            if(this._type){
                self.model.findType(this._type).then(function(data){
                    self._view.display(data, false);
                });
            }else{
                self.model.findAll().then(function(data){
                    self._view.display(data, false);
                });
            }
        }else{
            this._view.display(this.model.cache.last, false);
        }
    }

    get view(){
        return this._view;
    }

    map(type){
        type = type?type:this._type;
        var map = new MapView();
        if(type){
            map.addMarks(this.model.cache[type]);
        }else{
            map.addMarks(this.model.cache.all);
        }
    }
}