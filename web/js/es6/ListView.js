/**
 * Created by francis on 17-6-5.
 */
class ListView extends AbstractView{
    constructor(pagination, options){
        super(pagination, options);
        this.name = 'list';
    }
    _models2HtmlStr(models){
        var self = this;
        var htmlStr = '';
        for(var model of models){
            var itemHtmlStr = `<div class="item">
                                  <div class="image">
                                    <img src="${model.img}">
                                  </div>
                                  <div class="content">
                                    <a class="header">${model.name}</a>
                                    <div class="meta">
                                      <span>${model.price}</span>
                                    </div>
                                  </div>
                                </div><!--end item-->`;
            htmlStr = htmlStr + itemHtmlStr;
        }
        return htmlStr;
    }
}