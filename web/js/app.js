/**
 * Created by francis on 17-6-4.
 */
$(document).ready(function () {
    var pagination = new Pagination();
    var listview = new ListView(pagination);
    var gridview = new GridView(pagination);
    var mapview = new MapView(pagination);
    var model = new Model(pagination);
    var controller = new Controller(model, listview);

    function init(){
        controller.index(0);
    }
    init();

    /**
     * change category event
     */
    $('.condition-area a').on('click tap', function(event){
        if(!$(this).hasClass('active')){
            var type = $(this).data('type');
            controller.type = type;
            controller.index(0);
        }
        return false;
    });
    /**
     * change view model event
     */
    $('.viewtypes a').on('click tap', function(event){
        if(!$(this).hasClass('active')){
            if($(this).data('view') == 'list'){
                controller.view = listview;
            }else if($(this).data('view') == 'grid'){
                controller.view = gridview;
            }else if($(this).data('view') == 'map'){
                // controller.map();
                controller.view = mapview;
            }
        }
        return false;
    });
    /**
     * click pager event
     */
    $(document).on('click tap','.ui.pagination.menu a.item', function(event){
        if(!$(this).hasClass('active')){
            var page = $(this).index();
            controller.index(page);
        }
        return false;
    })
});