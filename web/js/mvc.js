'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by francis on 17-6-5.
 */
var Pagination = function () {
    function Pagination(options) {
        _classCallCheck(this, Pagination);

        this.settings = $.extend({ linkNext: '.next', linkPrev: '.prev',
            linkFirst: '.first', linkLast: '.last', pageSize: 20, maxButtonCount: 10 }, options || {});
        this.pageSize = this.settings.pageSize;
        this._page = 0;
        this.maxButtonCount = this.settings.maxButtonCount;
    }

    _createClass(Pagination, [{
        key: 'totalCount',
        get: function get() {
            return this._totalCount;
        },
        set: function set(number) {
            this._totalCount = number;
        }
        /**
         * @return int number of pages
         */

    }, {
        key: 'pageCount',
        get: function get() {
            if (this.pageSize < 1) {
                return this.totalCount > 0 ? 1 : 0;
            } else {
                var totalCount = this.totalCount < 0 ? 0 : this.totalCount;
                return Math.floor((totalCount + this.pageSize - 1) / this.pageSize);
            }
        }

        /**
         * Returns the zero-based current page number.
         * @return int the zero-based current page number.
         */

    }, {
        key: 'page',
        get: function get() {
            return this._page;
        },
        set: function set(val) {
            // val = val-1;
            if (val < 0) {
                val = 0;
            }
            var pagecount = this.pageCount;
            if (val >= pagecount) {
                val = pagecount - 1;
            }
            this._page = val;
        }

        /**
         * @return int the offset of the data.
         */

    }, {
        key: 'offset',
        get: function get() {
            var pagesize = this.pageSize;
            return pagesize < 1 ? 0 : this.page * pagesize;
        }
    }, {
        key: 'limit',
        get: function get() {
            var pagesize = this.pageSize;
            // console.log('limit:',pagesize);
            return pagesize < 1 ? -1 : pagesize;
        }
    }, {
        key: 'pageRange',
        get: function get() {
            var currentPage = this.page;
            var pageCount = this.pageCount;
            var beginPage = Math.max(0, currentPage - Math.floor(this.maxButtonCount / 2));
            var endPage = null;
            if ((endPage = beginPage + this.maxButtonCount - 1) >= pageCount) {
                endPage = pageCount - 1;
                beginPage = Math.max(0, endPage - this.maxButtonCount + 1);
            }
            return [beginPage, endPage];
        }
    }]);

    return Pagination;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by francis on 17-6-5.
 */
var Model = function () {
    function Model(pagination) {
        _classCallCheck(this, Model);

        var self = this;
        self.data_file = 'data.json';
        self.pagination = pagination;
        self.data = $.getJSON(self.data_file);
        self.cache = {};
        self.cache.last = null; //last found items, pagination pageSized items.
        self.cache.all = null;
        self.cache.food = null;
        self.cache.movie = null;
    }

    _createClass(Model, [{
        key: '_paginationCut',
        value: function _paginationCut(data, condition) {
            var self = this;
            self.pagination.totalCount = data.length;
            self.pagination.page = condition.page;
            console.log('self.pagination.page:', self.pagination.page);
            console.log('self.pagination.offset:', self.pagination.offset);
            var pagination = self.pagination;
            console.log('pagination.offset:', pagination.offset);
            data = data.slice(pagination.offset, pagination.offset + pagination.limit);
            self.cache.last = data;
            return data;
        }
    }, {
        key: 'find',
        value: function find(condition) {
            var self = this;
            var defer = $.Deferred();
            if (condition.type) {
                this.findType(condition.type).then(function (data) {
                    data = self._paginationCut(data, condition);
                    defer.resolve(data);
                });
            } else {
                this.findAll().then(function (data) {
                    data = self._paginationCut(data, condition);
                    defer.resolve(data);
                });
            }
            return defer;
        }
    }, {
        key: 'findAll',
        value: function findAll() {
            var self = this;
            var defer = $.Deferred();
            if (self.cache.all) {
                self.pagination.totalCount = self.cache.all.length;
                defer.resolve(self.cache.all);
            } else {
                self.data.done(function (data) {
                    var items = data.items;
                    self.cache.all = items;
                    self.pagination.totalCount = self.cache.all.length;
                    defer.resolve(self.cache.all);
                });
            }
            return defer;
        }
    }, {
        key: 'findType',
        value: function findType(type) {
            var self = this;
            var defer = $.Deferred();
            if (self.cache[type]) {
                self.pagination.totalCount = self.cache[type].length;
                defer.resolve(self.cache[type]);
            } else {
                self.data.done(function (data) {
                    var items = data.items.filter(function (elem, index, self) {
                        return elem.type == type;
                    });
                    self.cache[type] = items;
                    self.pagination.totalCount = items.length;
                    defer.resolve(self.cache[type]);
                });
            }
            return defer;
        }
    }]);

    return Model;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by francis on 17-6-4.
 */
var AbstractView = function () {
    function AbstractView(pagination, options) {
        _classCallCheck(this, AbstractView);

        this.settings = $.extend({ listContainer: '#itemlist', pagerCountainer: '.pager',
            totalCount: '.total .count' }, options || {});
        this.pagination = pagination;
        this.name = 'abstract';
    }

    _createClass(AbstractView, [{
        key: 'displayTotalCount',
        value: function displayTotalCount() {
            console.log('this.pagination.totalCount:', this.pagination.totalCount);
            $(this.settings.totalCount).text(this.pagination.totalCount);
        }
    }, {
        key: 'displayPager',
        value: function displayPager() {
            var $pages = this._pageButtons();
            $(this.settings.pagerCountainer).empty().append($pages);
        }
    }, {
        key: '_pageButtons',
        value: function _pageButtons() {
            var $container = $('<div>').addClass('ui pagination menu');
            var pagerange = this.pagination.pageRange;
            for (var i = pagerange[0]; i <= pagerange[1]; i++) {
                var $btn = $('<a>').addClass('item').text(i + 1);
                if (i == this.pagination.page) {
                    $btn.addClass('active');
                }
                $container.append($btn);
            }
            return $container;
        }
    }, {
        key: 'replaceProducts',
        value: function replaceProducts(items) {
            var self = this;
            $(self.settings.listContainer).empty().removeAttr('style');
            var htmlString = this._models2HtmlStr(items);
            if (htmlString) $(self.settings.listContainer).html(htmlString);else {
                $(self.settings.listContainer).html(this.constructor.noSearchData);
            }
            window.scrollTo(0, 0);
        }
    }, {
        key: 'appendProducts',
        value: function appendProducts(items) {
            var self = this;
            var htmlString = this._models2HtmlStr(items);
            if (htmlString) $(self.settings.listContainer).append(htmlString);
        }
    }, {
        key: 'display',
        value: function display(items) {
            var append = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            this.displayTotalCount();
            if (append) {
                this.appendProducts(items);
            } else {
                this.replaceProducts(items);
            }
            this.displayPager();
        }
    }, {
        key: '_models2HtmlStr',
        value: function _models2HtmlStr(models) {}
    }], [{
        key: 'noSearchData',
        get: function get() {
            return '<li class="no-data">\u5F53\u524D\u67E5\u8BE2\u6761\u4EF6\u4E0B\u6CA1\u6709\u4FE1\u606F\uFF0C\u53BB\u8BD5\u8BD5\u5176\u4ED6\u67E5\u8BE2\u6761\u4EF6\u5427\uFF01</li>';
        }
    }]);

    return AbstractView;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by francis on 17-6-5.
 */
var ListView = function (_AbstractView) {
    _inherits(ListView, _AbstractView);

    function ListView(pagination, options) {
        _classCallCheck(this, ListView);

        var _this = _possibleConstructorReturn(this, (ListView.__proto__ || Object.getPrototypeOf(ListView)).call(this, pagination, options));

        _this.name = 'list';
        return _this;
    }

    _createClass(ListView, [{
        key: '_models2HtmlStr',
        value: function _models2HtmlStr(models) {
            var self = this;
            var htmlStr = '';
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = models[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var model = _step.value;

                    var itemHtmlStr = '<div class="item">\n                                  <div class="image">\n                                    <img src="' + model.img + '">\n                                  </div>\n                                  <div class="content">\n                                    <a class="header">' + model.name + '</a>\n                                    <div class="meta">\n                                      <span>' + model.price + '</span>\n                                    </div>\n                                  </div>\n                                </div><!--end item-->';
                    htmlStr = htmlStr + itemHtmlStr;
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            return htmlStr;
        }
    }]);

    return ListView;
}(AbstractView);
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Created by francis on 17-6-6.
 */
var GridView = function (_AbstractView) {
    _inherits(GridView, _AbstractView);

    function GridView(pagination, options) {
        _classCallCheck(this, GridView);

        var _this = _possibleConstructorReturn(this, (GridView.__proto__ || Object.getPrototypeOf(GridView)).call(this, pagination, options));

        _this.name = 'grid';
        return _this;
    }

    _createClass(GridView, [{
        key: '_models2HtmlStr',
        value: function _models2HtmlStr(models) {
            var self = this;
            var $container = $('<div>').addClass('ui four stackable cards');
            var htmlStr = '';
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = models[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var model = _step.value;

                    var itemHtmlStr = '<div class="ui card">\n          <div class="ui slide masked reveal image">\n            <img src="' + model.img + '">\n          </div>\n          <div class="content">\n            <a class="header">' + model.name + '</a>\n            <div class="meta">\n              <span class="date">' + model.price + '</span>\n            </div>\n          </div>\n        </div><!--end item-->';
                    htmlStr = htmlStr + itemHtmlStr;
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }

            $container.append(htmlStr);
            return $container;
        }
    }]);

    return GridView;
}(AbstractView);
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by francis on 17-6-6.
 */
var MapView = function () {
    function MapView(pagination, options) {
        _classCallCheck(this, MapView);

        this.settings = $.extend({ container: 'itemlist', containerHeight: 600, pagerCountainer: '.pager',
            totalCount: '.total .count' }, options || {});
        this.pagination = pagination;
        this.name = 'map';
    }

    _createClass(MapView, [{
        key: 'init',
        value: function init() {
            $(this.settings.pagerCountainer).empty();
            $('#' + this.settings.container).height(this.settings.containerHeight);
            this.map = new BMap.Map(this.settings.container);
            this.map.enableScrollWheelZoom(true);
            this.centerPoint = new BMap.Point(116.404, 39.915);
            this.map.centerAndZoom(this.centerPoint, 15);
        }
    }, {
        key: 'displayTotalCount',
        value: function displayTotalCount() {
            console.log('this.pagination.totalCount:', this.pagination.totalCount);
            $(this.settings.totalCount).text(this.pagination.totalCount);
        }
    }, {
        key: 'display',
        value: function display(items) {
            this.displayTotalCount();
            this.addMarks(items);
        }
    }, {
        key: 'addMarks',
        value: function addMarks(items) {
            var self = this;
            self.map.clearOverlays();
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = items[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var item = _step.value;

                    var point = new BMap.Point(item.coord.x, item.coord.y);
                    var html = {};
                    html.food = '<div class="mapmarker ' + item.type + '"><i class="food icon"></i>' + item.name + ' ' + item.price + '</div>';
                    html.movie = '<div class="mapmarker ' + item.type + '"><i class="record icon"></i>' + item.name + ' ' + item.price + '</div>';
                    var marker = new BMapLib.RichMarker(html[item.type], point);
                    self.map.addOverlay(marker);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
        /*    _addFoodMarks(items){
        
            }
            _addMovieMarks(items){
        
            }*/

    }]);

    return MapView;
}();
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Created by francis on 17-6-6.
 */
var Controller = function () {
    function Controller(model, view, type) {
        _classCallCheck(this, Controller);

        this._view = view;
        this.model = model;
        this._type = type;
    }

    /**
     * render index page
     * @param integer page current page number
     * @param string type
     */


    _createClass(Controller, [{
        key: 'index',
        value: function index(page) {
            var self = this;
            if (self._view.name == 'map') {
                if (this._type) {
                    self.model.findType(this._type).then(function (data) {
                        self._view.display(data, false);
                    });
                } else {
                    self.model.findAll().then(function (data) {
                        self._view.display(data, false);
                    });
                }
            } else {
                self.model.find({ page: page, type: self._type }).then(function (data) {
                    self._view.display(data, false);
                });
            }
        }
    }, {
        key: 'map',
        value: function map(type) {
            type = type ? type : this._type;
            var map = new MapView();
            if (type) {
                map.addMarks(this.model.cache[type]);
            } else {
                map.addMarks(this.model.cache.all);
            }
        }
    }, {
        key: 'type',
        set: function set(val) {
            this._type = val;
        }
    }, {
        key: 'view',
        set: function set(view) {
            var self = this;
            this._view = view;
            if (self._view.name == 'map') {
                self._view.init();
                if (this._type) {
                    self.model.findType(this._type).then(function (data) {
                        self._view.display(data, false);
                    });
                } else {
                    self.model.findAll().then(function (data) {
                        self._view.display(data, false);
                    });
                }
            } else {
                this._view.display(this.model.cache.last, false);
            }
        },
        get: function get() {
            return this._view;
        }
    }]);

    return Controller;
}();