/**
 * Created by francis on 17-5-11.
 */
const gulp = require('gulp');
const less = require('gulp-less');
const autoprefixer = require('gulp-autoprefixer');
const minifyCSS    = require('gulp-clean-css');
const babel = require("gulp-babel");
const concat = require('gulp-concat');

/*******************************
 Tasks
 *******************************/

var src_less = './*.less';
var dist = './web/css';

gulp.task('css', function () {
    return gulp.src(src_less)
        .pipe(less())
        // .pipe(minifyCSS())
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(gulp.dest(dist));
});

gulp.task('watchcss', ['css'], function() {
    gulp.watch(src_less, ['less']);
});

var jsfiles = [
    'web/js/es6/Pagination.js',
    'web/js/es6/Model.js',
    'web/js/es6/AbstractView.js',
    'web/js/es6/ListView.js',
    'web/js/es6/GridView.js',
    'web/js/es6/MapView.js',
    'web/js/es6/Controller.js',
];
gulp.task('js',function(){
    gulp.src(jsfiles)
        .pipe(babel({presets: ['es2015']}))
        // .pipe(stripDebug())
        .pipe(concat('mvc.js'))
        // .pipe(uglify({'mangle':false}))
        .pipe(gulp.dest('web/js/'));
});