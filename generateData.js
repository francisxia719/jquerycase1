/**
 * Created by francis on 17-6-3.
 */
var fs = require('fs');
var futilities = require('./mylib/fxutilities.js');

var data_file = 'web/data.json';
var numbers = process.argv[2]?process.argv[2]:100;

var food_types = [
    {name:'火锅',min:40,max:120,img:'/images/huoguo.jpg'},
    {name:'麻辣烫',min:20,max:40,img:'/images/malatang.jpg'},
    {name:'烤鱼',min:40,max:100,img:'/images/kaoyu.jpg'},
    {name:'大虾',min:60,max:150,img:'/images/daxia.jpg'},
    {name:'炖菜',min:45,max:90,img:'/images/duncai.jpg'},
    {name:'麻辣香锅',min:30,max:70,img:'/images/malaxiangguo.jpg'},
    {name:'黄焖鸡米饭',min:30,max:45,img:'/images/huangmenjimifan.jpg'},
    {name:'自助餐',min:15,max:60,img:'/images/zizhucan.jpg'},
    {name:'煲仔饭',min:32,max:47,img:'/images/baozaifan.jpg'},
    {name:'过桥米线',min:18,max:48,img:'/images/mixian.jpg'},
    {name:'米皮',min:15,max:25,img:'/images/mipi.jpg'},
    {name:'凉皮',min:15,max:25,img:'/images/liangpi.jpg'},
    {name:'果木烤鸭',min:25,max:67,img:'/images/kaoya.jpg'},
    {name:'寿司',min:25,max:40,img:'/images/shousi.jpg'},
    {name:'大盆骨',min:40,max:80,img:'/images/dapengu.jpg'},
    {name:'自助烤肉',min:40,max:80,img:'/images/kaorou.jpg'}
    ];
var food_brands = [{name:'鑫海汇',img:'/images/xinhaihui.jpg'},
    {name:'海丽轩',img:''},
    {name:'木槿花韩式',img:''},
    {name:'京成一品欢乐',img:''},{name:'韩鲜坊',img:''},
    {name:'丁丁洋',img:'/images/dingdingyang.jpg'},
    {name:'状元',img:''},
    {name:'美羊羊',img:'/images/meiyangyang.jpg'},{name:'王婆',img:'/images/wangpo.jpg'},
    {name:'西秀',img:''},{name:'本味鲜',img:''},
    {name:'韩风源',img:''},{name:'鲁班',img:''},{name:'爵士',img:''},{name:'佳客来',img:''},
    {name:'珍味坊',img:'/images/zhenweifang.jpg'},
    {name:'快食坊',img:''},{name:'豫碗香',img:''},{name:'解家',img:''},{name:'小肥羊',img:''},{name:'永和铂爵',img:''},
    {name:'百岁鱼',img:''},{name:'金厨坊',img:''},{name:'重庆德庄',img:''},
    {name:'三锅演义',img:'/images/sanguoyanyi.jpg'},
    {name:'金壮壮',img:''},{name:'胖哥',img:''},{name:'汉釜宫',img:'/images/hanfugong.jpg'},
    {name:'鱼味巴蜀',img:''},{name:'希尔顿',img:''},
    {name:'八元素',img:''},{name:'阿里',img:''},{name:'齐祺',img:''},{name:'惠丰',img:''},
    {name:'德庄一品',img:''},
    {name:'一品',img:''},{name:'顶呱呱',img:''},{name:'好运来',img:''}
];

var movies=[
    {name:'异星觉醒',min:40, max:80,img:'/images/yixingjuexing.jpg'},
    {name:'奇异博士',min:35, max:85,img:'/images/qiyiboshi.jpg'},
    {name:'血战钢锯岭',min:35, max:85,img:'/images/xuezhangangjuling.jpg'},
    {name:'金刚狼3：殊死一战',min:35, max:85,img:'/images/shusiyizhan.jpg'},
    {name:'疾速特攻',min:35, max:85,img:'/images/jisutegong.jpg'}];
var cinema=[
    {name:'美嘉欢乐影城中关村店',img:''},{name:'星美国际影城',img:''},{name:'新影联·华谊兄弟影院',img:''},
    {name:'北京耀莱成龙国际影城（五棵松店）',img:''}, {name:'金逸国际影城（朝阳大悦城店）',img:''},
    {name:'首都电影院（西单店）',img:''},{name:'北京万达影城通州万达广场店',img:''},
    {name:'中影国际影城北京丰台千禧街店',img:''},{name:'UME国际影城',img:''},{name:'劲松电影院',img:''},
    {name:'海淀工人文化宫',img:''},{name:'大地影院',img:''},{name:'博纳国际影城土桥店',img:''},{name:'北京紫光影城',img:''},
];

var types= [
    {name:'food',products:food_types,brands:food_brands},
    {name:'movie',products:movies,brands:cinema}
];

//116.238406,39.995464       116.508616,39.790415
var minX = 116.238406;
var maxX = 116.508616;
var minY = 39.790415;
var maxY = 39.995464;
var items = [];
for(var i=0; i<numbers; i++){
    var type = futilities.fxtools.randomArrayElement(types);
    var brand = futilities.fxtools.randomArrayElement(type.brands);
    var product = futilities.fxtools.randomArrayElement(type.products);
    var name = `${brand.name} ${product.name}`;
    var price = futilities.math.randomPrecision(product.min, product.max, 1);
    var img = brand.img?brand.img:product.img;
    var x = futilities.math.randomPrecision(minX, maxX, 6);
    var y = futilities.math.randomPrecision(minY, maxY, 6);
    img = img?img:'/images/default.jpg';
    var item = {id:i+1,type:type.name,price:price,name:name,coord:{x:x,y:y},img:img};
    items.push(item);
    console.log(item);
    // fs.appendFileSync(data_file, item);
}
var str = JSON.stringify({items:items});
fs.writeFileSync(data_file, str);
